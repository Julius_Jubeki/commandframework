package de.jubeki.cmdframework.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class PlayerCommand extends Command {

	public PlayerCommand(String name, Command parent) {
		super(name, parent);
	}

	@Override
	public boolean execute(CommandSender cs, String label, String[] args) {
		return execute((Player)cs, label, args);
	}
	
	public abstract boolean execute(Player p, String label, String[] args);
	
	@Override
	public boolean checkPermissions(CommandSender cs) {
		if(cs instanceof Player == false) return false;
		return super.checkPermissions(cs);
	}

}
