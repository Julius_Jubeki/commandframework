package de.jubeki.cmdframework.command;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public abstract class ConsoleCommand extends Command {

	public ConsoleCommand(String name, Command parent) {
		super(name, parent);
	}

	@Override
	public boolean execute(CommandSender cs, String label, String[] args) {
		return execute((ConsoleCommandSender) cs, label, args);
	}
	
	public abstract boolean execute(ConsoleCommandSender console, String label, String[] args);
	
	@Override
	public boolean checkPermissions(CommandSender cs) {
		return cs instanceof ConsoleCommandSender;
	}

}
