package de.jubeki.cmdframework.command;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.jubeki.cmdframework.JubeUtil;
import de.jubeki.cmdframework.parent.AbstractCommand;

public abstract class Command extends AbstractCommand implements Comparable<Command> {

	private final Command parentCommand;
	protected ArrayList<Command> subcommands = new ArrayList<>();
	private String usage;

	public Command(String name, Command parent) {
		super(name.split(" ")[0]);
		this.parentCommand = parent;
	}
	
	public Command(String name) {
		this(name, null);
	}
	
	public abstract boolean execute(CommandSender cs, String label, String[] args);
	
	@Override
	public int compareTo(Command cmd) {
		return getFullCommand().compareToIgnoreCase(cmd.getFullCommand());
	}
	
	public void onCommand(CommandSender cs, String label, String[] args) {
		if(checkForSubCommands(cs, label, args) == true) return;
		if(checkPermissions(cs) == false) {
			cs.sendMessage(getNoPermissionsMessage());
			return;
		}
		if(checkArguments(args) == false) {
			cs.sendMessage("�c" + getUsage());
			return;
		}
		if(execute(cs, label, args) == false) cs.sendMessage("�c" + getUsage());
	}
	
	public ArrayList<String> onTabComplete(CommandSender cs, String[] args, boolean space) {
		ArrayList<String> complete = new ArrayList<>();
		if(space && subcommands.size() > 0 && args.length == 0) {
			for(Command cmd : subcommands) {
				if(cmd.checkPermissions(cs) == false) continue;
				complete.add(cmd.getName());
				for(String s : cmd.getLabel()) {
					complete.add(s);
				}
				cs.sendMessage(cmd.getName());
			}
			return complete;
		} else if(space == false && args.length == 1 && subcommands.size() > 0) {
			for(Command cmd : subcommands) {
				if(cmd.getName().toLowerCase().startsWith(args[0].toLowerCase()) || JubeUtil.startsArrayWith(args[0], getLabel())) {
					if(cmd.checkPermissions(cs) == false) continue;
					complete.add(cmd.getName()) ;
				}
			}
			return complete;
		} else if(args.length > 0 && subcommands.size() > 0) {
			for(Command cmd : subcommands) {
				if(cmd.getName().equalsIgnoreCase(args[0]) || JubeUtil.hasArrayString(args[0], cmd.getLabel())) {
					if(cmd.checkPermissions(cs) == false) return complete;
					String[] args2 = new String[args.length-1];
					System.arraycopy(args, 1, args2, 0, args2.length);
					return cmd.onTabComplete(cs, args2, space);
				}
			}
			return complete;
		}
		if(checkPermissions(cs) == false) return complete;
		if(args.length > getTabComplete().length || args.length == getTabComplete().length && space) return complete;
		int index = 0;
		if(args.length > 0 && args.length <= getTabComplete().length) {
			if(space && args.length != getTabComplete().length) {
				index = args.length;
			} else {
				index = args.length-1;
			}
		}
		complete.addAll(replaceOnTabComplete(cs, getTabComplete()[index], args, space));
		return complete;
	}

	public boolean checkArguments(String[] args) {
		if(args.length < getMinArgs() || getMaxArgs() != -1 && args.length > getMaxArgs()) return false;
		return true;
	}
	
	public boolean checkForSubCommands(CommandSender cs, String label, String[] args) {
		if(args.length == 0) return false;
		for(Command cmd : subcommands) {
			if(cmd.getName().toLowerCase().equals(args[0].toLowerCase()) || JubeUtil.hasArrayString(cmd.getName(), args)) {
				String[] args2 = new String[args.length-1];
				System.arraycopy(args, 1, args2, 0, args2.length);
				cmd.onCommand(cs, label, args2);
				return true;
			}
		}
		return false;
	}

	public boolean checkPermissions(CommandSender cs) {
		if(cs instanceof ConsoleCommandSender || getPermissions() == null || getPermissions().length == 0) return true;
		for(String permission : getPermissions()) {
			if(cs.hasPermission(permission)) return true;
		}
		return false;
	}

	public ArrayList<Command> getSubCommands() {
		return subcommands;
	}

	public void setSubCommands(ArrayList<Command> subcommands) {
		if(subcommands == null) throw new NullPointerException();
		this.subcommands = subcommands;
	}
	
	public void addSubCommand(Command subcmd) {
		if(subcmd == null) throw new NullPointerException();
		subcommands.add(subcmd);
	}
	
	public ArrayList<String> replaceOnTabComplete(CommandSender cs, String s, String[] args, boolean space) {
		ArrayList<String> complete = new ArrayList<>();	
		if(s.equalsIgnoreCase("%player")) {
			for(Player p : Bukkit.getOnlinePlayers()) {
				complete.add(p.getName());
			}
		} else if(cs instanceof Player && s.startsWith("%")) {
			if(s.equalsIgnoreCase("%locWorld")) {
				complete.add(((Player)cs).getLocation().getWorld().getName());
			} else if(s.equalsIgnoreCase("%loc")) {
				int add = 0;
				if(space) {
					if(args.length >= 2) {
						try {
							Double.parseDouble(args[args.length-2]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
					} else if(args.length >= 1) {
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {}
					}
				} else {
					if(args.length >= 3) {
						try {
							Double.parseDouble(args[args.length-3]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
						try {
							Double.parseDouble(args[args.length-2]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
					} else if(args.length >= 2) {
						try {
							Double.parseDouble(args[args.length-2]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
					} else {
						try {
							Double.parseDouble(args[args.length-1]);
							add++;
						} catch(NumberFormatException e) {
							add = 0;
						}
					}
				}
				if(add == 2) {
					complete.add(String.valueOf(((Player)cs).getLocation().getBlockZ()));
				} else if(add == 1) {
					complete.add(String.valueOf(((Player)cs).getLocation().getBlockY()));
				} else {
					complete.add(String.valueOf(((Player)cs).getLocation().getBlockX()));
				}
			} else if(s.equalsIgnoreCase("%locX")) {
				complete.add(String.valueOf(((Player)cs).getLocation().getX()));
			} else if(s.equalsIgnoreCase("%locY")) {
				complete.add(String.valueOf(((Player)cs).getLocation().getY()));
			} else if(s.equalsIgnoreCase("%locZ")) {
				complete.add(String.valueOf(((Player)cs).getLocation().getZ()));
			} else {
				complete.add(s);
			}
		} else {
			if(space) {
				complete.add(s);
			} else if(args.length > 0){
				if(s.toLowerCase().startsWith(args[args.length-1].toLowerCase())) {
					complete.add(s);
				}
			}
		}
		return complete;
	}

	public ArrayList<String> replaceOnTabComplete(CommandSender cs, String[] strs, String[] args, boolean space) {
		ArrayList<String> complete = new ArrayList<>();
		for(String s : strs) {
			complete.addAll(replaceOnTabComplete(cs, s, args, space));
		}
		return complete;
	}

	public Command getParentCommand() {
		return parentCommand;
	}
	
	public boolean hasParentCommand() {
		return parentCommand != null;
	}
	
	public String getFullCommand() {
		if(hasParentCommand()) {
			return parentCommand.getFullCommand() + " " + getName();
		}
		return getName();
	}

	public String getUsage() {
		return usage == null ? "/" + getFullCommand() : usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

}
