package de.jubeki.cmdframework.help;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import org.bukkit.command.CommandSender;

import de.jubeki.cmdframework.CommandFramework;
import de.jubeki.cmdframework.command.Command;

public class HelpCommand extends Command {
	
	private final int max_page;
	private final Command[] commands;

	public HelpCommand(String name, CommandFramework cmdframework) {
		super(name);
		setDescription("Help page for the plugin");
		setMinArgs(0);
		setMaxArgs(1);
		setNoPermissionsMessage("�cYou don't habe permissions to see this plugin help!");
		setUsage("/" + name + " [page]");
		setTabComplete(new String[] {"%help"});
		LinkedList<Command> linkedcommands = new LinkedList<>();
		setData(linkedcommands, cmdframework.getCommands());
		Collections.sort(linkedcommands);
		commands = linkedcommands.toArray(new Command[linkedcommands.size()]);
		this.max_page = commands.length == 0 ? 0 : commands.length/6+1;
	}

	private void setData(LinkedList<Command> lcmd, Collection<Command> commands) {
		for(Command cmd : commands) {
			lcmd.add(cmd);
			setData(lcmd, cmd.getSubCommands());
		}
	}

	@Override
	public boolean execute(CommandSender cs, String label, String[] args) {
		int page = 1;
		if(args.length == 1) {
			try {
				page = Integer.parseInt(args[0]);
			} catch(NumberFormatException e) {
				cs.sendMessage("�cPlease enter a number!");
				return true;
			}
		}
		if(this.max_page == 0) {
			cs.sendMessage("�cNo commands registered!");
			return true;
		}
		if(page > this.max_page || page < 1) {
			page = 1;
		}
		cs.sendMessage("�2===================[ �aHelp (p. " + page + "/" + max_page + ") �2]===================");
		try {
			for(int i = (page-1)*5; i < page*5; i++) {
				StringBuilder sb = new StringBuilder("�e");
				sb.append(commands[i].getUsage());
				sb.append("�7 � ");
				sb.append(commands[i].getDescription());
				cs.sendMessage(sb.toString());
			}
		} catch(IndexOutOfBoundsException e) {}
		cs.sendMessage("�2===================[ �aHelp (p. " + page + "/" + max_page + ") �2]===================");
		return true;
	}
	
	@Override
	public ArrayList<String> replaceOnTabComplete(CommandSender cs, String s, String[] args, boolean space) {
		ArrayList<String> complete = new ArrayList<>();
		if(s.equals("%help")) {
			for(int i = 1; i <= max_page; i++) {
				String number = String.valueOf(i);
				if(space == false && number.startsWith(args[0]) == false) continue;
				complete.add(number);
			}
			return complete;
		}
		return super.replaceOnTabComplete(cs, s, args, space);
	}
}
