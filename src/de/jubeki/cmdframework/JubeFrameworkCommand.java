package de.jubeki.cmdframework;

import org.bukkit.command.CommandSender;

import de.jubeki.cmdframework.command.Command;

public class JubeFrameworkCommand extends Command {

	public JubeFrameworkCommand() {
		super("jubeCommandFramework");
		setMinArgs(0);
		setMaxArgs(0);
		setDescription("Shows you the version of this plugin");
		setPermissions("jcf.version");
	}

	@Override
	public boolean execute(CommandSender cs, String label, String[] args) {
		cs.sendMessage("�cName: �eJubeCommandFramework");
		cs.sendMessage("�cAuthor: �eJubeki");
		cs.sendMessage("�cVersion: �e" + JubeFramework.getInstance().getDescription().getVersion());
		return true;
	}

}
