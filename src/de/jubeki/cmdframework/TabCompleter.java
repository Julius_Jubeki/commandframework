package de.jubeki.cmdframework;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

import de.jubeki.cmdframework.command.Command;

public class TabCompleter implements Listener {
	
	private final CommandFramework cmdframwork;
	
	public TabCompleter(CommandFramework commandFramework) {
		this.cmdframwork = commandFramework;
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void on(TabCompleteEvent e) {
		if(e.getSender() instanceof ConsoleCommandSender) return;
		String[] args = e.getBuffer().split(" ");
		boolean space = e.getBuffer().endsWith(" ");
		if(!e.getBuffer().startsWith("/") || args.length == 0) {
			return;
		}
		if(args.length == 1 && space == false) {
			List<String> complete = e.getCompletions();
			for(Command cmd : cmdframwork.getCommands()) {
				if(cmd.getName().toLowerCase().startsWith(args[0].substring(1).toLowerCase()) || JubeUtil.startsArrayWith(args[0].substring(1), cmd.getLabel())) {
					if(cmd.checkPermissions(e.getSender()) == true) {
						complete.add("/" + cmd.getName());
						for(String s : cmd.getLabel()) {
							complete.add("/" + s);
						}
					}
				}
			}
			e.setCompletions(complete);
			return;
		}
		
		ArrayList<String> complete = new ArrayList<>();
		for(Command cmd : cmdframwork.getCommands()) {
			String command = "/" + cmd.getName();
			if(command.equalsIgnoreCase(args[0]) || JubeUtil.hasArrayString(args[0].substring(1, args[0].length()), cmd.getLabel())) {
				String[] args2 = new String[args.length-1];
				System.arraycopy(args, 1, args2, 0, args2.length);
				complete.addAll(cmd.onTabComplete(e.getSender(), args2, space));
				e.setCompletions(complete);
				return;
			}
		}
	}

}
