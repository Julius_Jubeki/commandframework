package de.jubeki.cmdframework;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.event.server.TabCompleteEvent;
import org.bukkit.plugin.java.JavaPlugin;

import de.jubeki.cmdframework.command.Command;
import de.jubeki.cmdframework.help.HelpCommand;

public abstract class CommandFramework extends JavaPlugin {
	
	private final HashMap<String, Command> commands;
	private final TabCompleter tabCompleter;
	private final CommandProcessor processor;
	private HelpCommand help;
	
	public CommandFramework() {
		this.commands = new HashMap<>();
		this.tabCompleter = new TabCompleter(this);
		this.processor = new CommandProcessor(this);
	}
	
	public final Set<Entry<String, Command>> getCommandEntrySet() {
		return commands.entrySet();
	}
	
	public final HashMap<String, Command> getCommandMap()	{
		return commands;
	}
	
	public final Collection<Command> getCommands() {
		return commands.values();
	}
	
	public final void registerCommand(Command cmd) {
		commands.put(cmd.getName().toLowerCase(), cmd);
	}
	
	public void registerHelp(String command, String... label) {
		String[] args = command.split(" ");
		help = new HelpCommand(args[0], this);
		help.setLabel(label);
		registerCommand(help);
	}
	
	public void sendHelp(CommandSender cs, int page) {
		if(help == null) return;
		help.execute(cs, help.getName(), new String[] {String.valueOf(page)});
	}
	
	@Override
	public final void onEnable() {
		Bukkit.getPluginManager().registerEvents(tabCompleter, this);
		Bukkit.getPluginManager().registerEvents(processor, this);
		enable();
	}
	
	@Override
	public final void onDisable() {
		PlayerCommandPreprocessEvent.getHandlerList().unregister(this);
		ServerCommandEvent.getHandlerList().unregister(this);
		TabCompleteEvent.getHandlerList().unregister(this);
		disable();
	}
	
	public abstract void enable();
	
	public abstract void disable();

}
