package de.jubeki.cmdframework;

import org.bukkit.plugin.java.JavaPlugin;

public class JubeFramework extends CommandFramework {

	@Override
	public void enable() {
		registerCommand(new JubeFrameworkCommand());
	}

	@Override
	public void disable() {
		
	}

	public static JubeFramework getInstance() {
		return JavaPlugin.getPlugin(JubeFramework.class);
	}

}
