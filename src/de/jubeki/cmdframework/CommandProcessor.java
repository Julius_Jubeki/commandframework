package de.jubeki.cmdframework;

import java.util.Map.Entry;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

import de.jubeki.cmdframework.command.Command;

public class CommandProcessor implements Listener {
	
	private final CommandFramework cmdframework;
	
	public CommandProcessor(CommandFramework cmdframework) {
		this.cmdframework = cmdframework;
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void on(PlayerCommandPreprocessEvent e) {
		String[] args = e.getMessage().split(" ");
		args[0] = args[0].substring(1, args[0].length());
		String name = args[0].toLowerCase();
		for(Entry<String, Command> entry : cmdframework.getCommandEntrySet()) {
			if(name.equals(entry.getKey().toLowerCase()) == false &&
					JubeUtil.hasArrayString(name, entry.getValue().getLabel()) == false) continue;
			e.setCancelled(true);
			String[] args2 = new String[args.length-1];
			System.arraycopy(args, 1, args2, 0, args2.length);
			entry.getValue().onCommand(e.getPlayer(), name, args2);
		}
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void on(ServerCommandEvent e) {
		String[] args = e.getCommand().split(" ");
		String name = args[0].toLowerCase();
		for(Entry<String, Command> entry : cmdframework.getCommandEntrySet()) {
			if(name.equals(entry.getKey().toLowerCase()) == false &&
					JubeUtil.hasArrayString(name, entry.getValue().getLabel()) == false) continue;
			e.setCancelled(true);
			String[] args2 = new String[args.length-1];
			System.arraycopy(args, 1, args2, 0, args2.length);
			entry.getValue().onCommand(e.getSender(), name, args2);
		}
	}
	
}
